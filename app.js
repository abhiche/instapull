var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var favicon = require('serve-favicon');

var ig = require('instagram-node').instagram();

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(__dirname + '/public/favicon.ico'));

var getUserBio = function (userId, callback) {
    ig.user(userId, function(err, result, remaining, limit) {
       if(err) {
           console.log(err);
           return callback(err, null);
       }
       return callback(null, result); 
    });  
};


app.get('/search', function(req, res) {
    
    var resultSet = [];
    
    var done = function() {
      return res.status(200).json(resultSet);  
    };
    
    ig.user_search(req.query.query, {count: 60} ,function(err, users, remaining, limit) {
        
        var counter=0;
        
        if(err) {
            
            if(err.code === 429 || err.code === 400 ) {
                return res.status(200).json({err: err.error_message});
            }
            
            return res.status(200).json({err: 'Unable to contact instagram server.'});
        }
        
        else {
        
            counter = users.length;
            
            if(counter === 0) {
                done([]);
            } else {
                
                var errorCounter = 0;
                
                users.forEach(function(element) {
                   
                   getUserBio(element.id, function(err, result) {
                       var user = {
                            bio: '',
                            website: '',
                            username: '',
                            full_name: '',
                            profile_picture: ''
                        };
                        
                        if(!err) {
                        
                        
                            if(typeof result !== 'undefined' && typeof result.full_name !== 'undefined' )
                                user.full_name = result.full_name;
                                
                            if(typeof result !== 'undefined' && typeof result.username !== 'undefined' )
                                user.username = result.username;
                                
                            if(typeof result !== 'undefined' && typeof result.profile_picture !== 'undefined' )
                                user.profile_picture = result.profile_picture;
                            
                           if(typeof result !== 'undefined' && typeof result.bio !== 'undefined' )
                                user.bio = result.bio;
                                
                           if(typeof result !== 'undefined' && typeof result.website !== 'undefined' )
                                user.website = result.website;
                           
                           if(user.bio === '' && user.website === '') {
                               //
                           } else {
                                resultSet.push(user);   
                           }
                            
                        } else {
            
                                if(err.code === 429) {
                                    if(++errorCounter === 1) {
                                        res.status(200).json({err: err.error_message});   
                                    }
                                    return false;
                                }
                        }
                        
                       
                       if(--counter === 0 && errorCounter < 1) {
                         done();   
                       }
                   });
                    
                });
                
                console.log('****************users.length***************', users.length);
                
            }
        }
        
    }); 
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500)
        .json({
        message: err.message,
        error: {}
    });
});

var port = process.env.INSTAPULL_PORT || 3000;

http.createServer(app).listen(port);

ig.use({ 
    client_id: '2df6d3a77eb44d8b83ab65bdc7575c56',
    client_secret: 'e45c963164a3472cb4d8e8311261d14d' 
});

module.exports = app;
