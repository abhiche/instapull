# Instapull
Application demonstrates how to fetch user information like username, fullname, bio, profile pic etc. from instagram.

Technologies Used:
NodeJS,
ExpressJS

Installation:

1. npm install
2. node app

Navigate to localhost:3000 from your web browser